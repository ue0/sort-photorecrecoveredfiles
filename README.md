# Sort recovered files

Inspired by [tfrdidi/sort-PhotorecRecoveredFiles](https://github.com/tfrdidi/sort-PhotorecRecoveredFiles)

When you restore files by photorec or another utility, you get a huge mess of unsorted files. manual sorting can take several days or more, this script will help you save your time.

### Zero step

First you need to recover your files. you can use this guide [PhotoRec Step By Step](https://www.cgsecurity.org/wiki/PhotoRec_Step_By_Step)

### Prepare

You need to download this script and install the dependencies.
```
git clone https://gitlab.com/ue0/sort-photorecrecoveredfiles.git
cd sort-photorecrecoveredfiles
pip install -r requirements.txt 
```

### Sorting

Run the script is very easy
```
python3 sort-files.py /path/to/folder/
```
This script sorts files by many parameters. It sorts documents and videos by extension, and moves them to the "+doc" and "+video" folders. audio files are sorted by author and album and moved to the "+music" folder, and rename the file by title. (example: +music/Gorilaz/Demon Days/68_Feel Good inc_f186557568.mp3). photo files are first sorted by size, and then by date of taking. (example: +photos/ok/2016:11/f201045792.jpg)
