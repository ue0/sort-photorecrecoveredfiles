import os
import sys
import shutil
import hashlib
from PIL import Image
from random import random
from tinytag import TinyTag

try:
    pwd = sys.argv[1]
except:
    pwd = os.getcwd()


filelist = []
for dirpath, dirnames, filenames in os.walk(pwd):
    for f in filenames:
        if os.path.isfile(os.path.join(dirpath, f)):
            filelist.append(os.path.join(dirpath, f))


def mv(source, destination):
    if os.path.exists(destination):
        if eqfile(source, destination):
            pass
        else:
            shutil.move(source, new_path(destination))
            print(source, "move to", new_path(destination))
    else:
        os.makedirs(os.path.dirname(destination), exist_ok=True)
        shutil.move(source, destination)
        print(source, "move to", destination)


def new_path(old):
    new = os.path.splitext(old)[0] + "_" + \
        str(random.randrange(99)) + os.path.splitext(old)[1]
    return new


def eqfile(file1, file2):
    if os.stat(file1).st_size and os.stat(file2).st_size < 5000000000:
        with open(file1, 'rb') as f1:
            data = f1.read()
            md5_1 = hashlib.md5(data).hexdigest()
        with open(file2, 'rb') as f2:
            data = f2.read()
            md5_2 = hashlib.md5(data).hexdigest()
        return md5_1 == md5_2
    else:
        return False


def sort_image(file):
    try:
        with Image.open(file) as im:
            pixels = im.size[0]*im.size[1]
            date = "" if im.getexif().get(306) is None else im.getexif().get(306)[:7]
    except:
        pass
    if pixels < 200000:
        size = "thumbnails"
    elif pixels > 2000000:
        size = "ok"
    else:
        size = "unsorted"
    destination = os.path.join(pwd, "+photos", size, date, os.path.basename(file))
    mv(file, destination)


def sort_video(file):
    destination = os.path.join(pwd, "+video", os.path.basename(file))
    mv(file, destination)


def sort_document(file):
    destination = os.path.join(pwd, "+doc", os.path.basename(file))
    mv(file, destination)


def sort_mp3(file):
    tag = TinyTag.get(file)
    artist = "" if tag.artist is None else tag.artist
    album = "" if tag.album is None else tag.album
    track = "" if tag.track is None else tag.track + "_"
    title = "" if tag.title is None else tag.title + "_"
    destination = os.path.join(pwd, "+music", artist, album, (track + title + os.path.basename(file)))
    mv(file, destination)


for f in filelist:
    extension = os.path.splitext(f)[1].lower()
    if extension in (".jpg", ".bmp", ".jpeg", ".png", ".tiff"):
        sort_image(f)
    elif extension in (".mp4", ".mov", ".3gp"):
        sort_video(f)
    elif extension in (".doc", ".docx", ".pdf"):
        sort_document(f)
    elif extension in (".mp3", ".mp4a", ".waw"):
        sort_mp3(f)
    else:
        pass
